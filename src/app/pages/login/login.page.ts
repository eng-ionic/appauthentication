import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthModel } from 'src/app/models/auth';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  authModel: AuthModel = {} as AuthModel;

  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  signup() {
    this.authService.signup(this.authModel).then(data => {
      if (data) {
        this.navCtrl.navigateRoot("/home");
      }
    })
    .catch(error => {
      alert('Ocorreu um erro ao se cadastrar.');
    })
  }

}
