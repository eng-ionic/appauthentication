import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthModel } from '../models/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient
  ) { }

  isLoggedIn(): boolean {
    const auth = localStorage.getItem('auth');
    if (!auth) return false;
    return true;
  }

  login() {

  }

  signup(model: AuthModel) {
    return new Promise((resolve, reject) => {
      this.http.post('http://performacomunicacao.inf.br/ionic/v_app_usuarios.php', {
        ...model,
        chave: 'joG2UzriVwserFRDES#LjXukJje8BOSvVOvKI',
        tipo: 1
      }).subscribe(data => {
        localStorage.setItem('auth', JSON.stringify({}));
        resolve(true);
      }, error => {
        reject(error);
      })
    })
  }
}
